package Vehicle;

public class Car extends Vehicle {
    private Boolean isConvertible;

    public Car(int maxSpeed, Boolean isConvertible) {
        super(maxSpeed);
        this.isConvertible = isConvertible;
    }

    public Boolean getConvertible() {
        return isConvertible;
    }

    public int tuneCar(int maxSpeedIncrease) {
        return (this.maxSpeed + maxSpeedIncrease);
    }

}
