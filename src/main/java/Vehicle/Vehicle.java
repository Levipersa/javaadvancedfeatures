package Vehicle;

public class Vehicle  {
    protected  final Integer maxSpeed;

    public Vehicle(){
        this(50);
    }
    public Vehicle(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }
}
