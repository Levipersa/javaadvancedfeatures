package exercitii;

public class Person {
    private String idCNP;
    private String nume;
    private String prenume;
    private int varsta;
    private String sex;
    private boolean retired;




    public boolean isRetired() {
        return this.retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public String getIdCNP() {
        return this.idCNP;
    }

    public void setIdCNP(String idCNP) {
        this.idCNP = idCNP;
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public int getVarsta() {
        return this.varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
