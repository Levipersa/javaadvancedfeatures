package exercitii;

import shape.Shape;

public class Circle extends Shape {
    private Integer radius;

    public Circle(){
        this(4);
        System.out.println("Constructor Circle called");
    }

    public Circle(Integer radius) {
        super(4);
        this.radius = radius;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public double calculateArea() {
        double area = Math.PI * Math.pow(radius,2);
        return area;
    }

    public void printColorCode(String color) {
        System.out.println("The color code is: " + getColorCode());
    }
}
