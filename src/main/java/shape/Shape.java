package shape;

public class Shape {
    protected Integer colorCode;

    public Shape(int test ) {
        System.out.println("Constructor Shape called");
    }

    public Integer getColorCode() {
        return colorCode;
    }

    public void setColorCode(Integer colorCode) {
        this.colorCode = colorCode;
    }
}
