package numbers;

public class TestNumber {

    private TestNumber() {
    }

    public static double calculateSum(Number[] numbers) {  //Number are toate wrapper class cu numere
        double sum = 0;
        for (Number nr : numbers) {
            sum += nr.doubleValue();
        }
        return sum;
    }

    //    Method overloading of calculateSum
    public static int calculateSum(Integer[] numbers) {  //Number are toate wrapper class cu numere
        int sum = 0;
        for (Number nr : numbers) {
            sum += nr.doubleValue();
        }
        return sum;
    }

    public static int calculateSum(String numbers,String delimiter) {
        String[] nrs = numbers.split(delimiter); //Am pus delimitator in parametru direct ....
        Integer[] vectorAux = new Integer[nrs.length];
        for (int i = 0; i < nrs.length; i++) {
            String currentNr = nrs[i];
            vectorAux[i] = Integer.parseInt(currentNr);
        }
        return calculateSum(vectorAux);

    }

}
